from django.db import models
from django.contrib.postgres.fields import JSONField, ArrayField
from django.contrib.auth.models import User
from django.utils.timezone import now

class Types(models.Model):
    name=models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)

class Ques_ans(models.Model):
    question=models.TextField()
    option1=models.CharField(max_length=2000,default="")
    option2=models.CharField(max_length=2000,default="")
    option3=models.CharField(max_length=2000,default="")
    option4=models.CharField(max_length=2000,default="")
    type_id=models.ForeignKey(Types,on_delete=models.CASCADE)
    corr_ans=JSONField(default=dict())
    
    def __str__(self):
        return str(self.question)

class Quiz(models.Model):
    name=models.CharField(max_length=100)
    ques_ids=ArrayField(models.CharField(max_length=25),default=list)

    def __str__(self):
        return str(self.name)

class Contest(models.Model):
    name=models.CharField(max_length=100)
    quiz_ids=ArrayField(models.CharField(max_length=25),default=list)
    def __str__(self):
        return str(self.name)


class Participation(models.Model):
    username=models.ForeignKey(User,on_delete=models.CASCADE)
    contest_part=models.ForeignKey(Contest,on_delete=models.CASCADE)
    quiz_allot=models.ForeignKey(Quiz,on_delete=models.CASCADE,null=True)
    contest_start_time=models.DateTimeField(default=now)

class QuizScore(models.Model):
    user_id=models.ForeignKey(User,on_delete=models.CASCADE)
    ques_id=models.ForeignKey(Ques_ans,on_delete=models.CASCADE)
    participation=models.ForeignKey(Participation,on_delete=models.CASCADE)
    solved=models.BooleanField(default=0)

    def __str__(self):
        return str(self.user_id)

class OnGoingcontest(models.Model):
    on_going=models.OneToOneField(Contest,on_delete=models.CASCADE)
    start_time=models.DateTimeField(default=now)
    end_time=models.DateTimeField(default=now)
    outisider_allowed = models.BooleanField(default=0)
