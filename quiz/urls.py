from django.conf.urls import url
from . import views

urlpatterns = [
    url('home/$',views.home,name='home'),
    url('contest/quiz/$', views.contest_page, name='question'),
    url('contest/quiz1/(?P<quesno>\w+)/$', views.ques_page, name='questionpage'),
    url('login_user/$',views.login_user,name='login-user'),
    url('questions/$', views.question, name='question'),
    url('answer/submit/$', views.submitans, name='submit-answer'),
    url('logout/$', views.logoutuser, name='logout'),

]
