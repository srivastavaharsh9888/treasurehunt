import datetime
import json
from datetime import timedelta
import random

from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render,redirect,HttpResponse
from .models import OnGoingcontest, Participation, Quiz, Ques_ans, QuizScore
from django.utils import timezone

def home(request):
    return render(request,'quiz/login.html',{})

@login_required
def contest_page(request):
    try:
        curr=OnGoingcontest.objects.get(pk=1)
        if (curr.start_time < timezone.now()) and (timezone.now() < curr.end_time):
            select_quiz_id=random.randint(1,len(curr.on_going.quiz_ids))
            select_quiz_obj=Quiz.objects.get(id=curr.on_going.quiz_ids[select_quiz_id-1])
            user,exists=Participation.objects.get_or_create(username=request.user,contest_part=curr.on_going)
            if exists:
                user.contest_start_time=timezone.now()
                user.quiz_allot = select_quiz_obj
                user.save()
            print(user.contest_start_time-timedelta(hours=6,minutes=30) ," ",curr.start_time-timedelta(hours=6,minutes=30) )
            print("Time after which user started the contest",((user.contest_start_time -curr.start_time)))
            print("time at which his contest will end",(user.contest_start_time + timedelta(minutes=30))-timedelta(hours=6,minutes=30))
            print("Contest end time ",curr.end_time-timedelta(hours=6,minutes=30))
            user_endtime=user.contest_start_time + timedelta(minutes=30)
            print("Current time",timezone.now()-timedelta(hours=6,minutes=30))
            if user_endtime > timezone.now():
                print("time is left",((user_endtime-timezone.now())))
            else:
                print("Time at which user tries again after his contest has end", timezone.now()-user_endtime)
            if (user.contest_start_time > curr.start_time) and   (timezone.now() < curr.end_time):
                    if user_endtime > timezone.now():
                        return redirect("/contest/quiz1/" + select_quiz_obj.ques_ids[0] + "/")
            print(select_quiz_id,select_quiz_obj.ques_ids[0],user.username)
            return HttpResponse("<h1>Your time has finished thanks for attending the quiz result will be announced soon.</h1>")
        else:
            if  (curr.start_time > timezone.now()):
                return HttpResponse("<h1>Sorry contest will soon.</h1>")
            if (curr.end_time < timezone.now()):
                return HttpResponse("<h1>Sorry contest end. Take part in other contest.</h1>")
    except Exception as e:
        return HttpResponse("<h1>Sorry no contest is live visit back later +"+ e + " </h1>")
    return render(request, 'quiz/question.html', {"userhere": request.user})

@login_required
def ques_page(request,quesno):
    curr = OnGoingcontest.objects.filter(pk=1).exists()
    if curr:
        try:
            curr = OnGoingcontest.objects.get(pk=1)
            user_playing=Participation.objects.get(contest_part=curr.on_going,username=request.user)
            if quesno in user_playing.quiz_allot.ques_ids:
                ques=Ques_ans.objects.get(pk=int(quesno))
                ques_ids = user_playing.quiz_allot.ques_ids
                all_question = Ques_ans.objects.filter(pk__in=ques_ids).values_list("pk","question","option1","option2","option3","option4")
                return render(request,'quiz/ques.html',{"questions":all_question,"total":all_question.count()})
            else:
                return HttpResponse("<h1>Sorry login and continue again.</h1>")
        except Exception as e:
            return HttpResponse("<h1>Login and try again.</h1>"+str(e))
    else:
        return HttpResponse("Sorry No On-going contest.")


def login_user(request):
    if not request.user.is_anonymous:
        logout(request)
        print("user logged out")
    curr = OnGoingcontest.objects.filter(pk=1).exists()
    if not curr:
        return HttpResponse("Sorry No On-going contest.")
    else:
        curr = OnGoingcontest.objects.get(pk=1)
        user=request.POST.get("email")
        password=request.POST.get("password")
        if not User.objects.filter(username=user).exists():
            return HttpResponse("Sorry User with this username does not exists.")
        user_obj=authenticate(username=user,password=password)
        if user_obj is None:
            return HttpResponse("Wrong Password Click <a href='/home/'>Here</a> to try again.")
        if curr.outisider_allowed:
            login(request,user_obj)
            return redirect("/contest/quiz/")
        elif user_obj.is_staff:
            login(request,user_obj)
            return redirect("/contest/quiz/")
        elif user_obj.is_staff==False and curr.outisider_allowed==False:
            return HttpResponse("Sorry you can't participate in this contest. This is only for the students of JUET.")
        else:
            return HttpResponse("Sorry you can't participate in this contest. This is only for the students of JUET.")


@login_required
def question(request):
    current=OnGoingcontest.objects.get(pk=1)
    participant_obj=Participation.objects.get(username=request.user,contest_part=current.on_going)
    ques_ids=participant_obj.quiz_allot.ques_ids
    all_question=Ques_ans.objects.filter(pk__in=ques_ids).values_list("pk")

    return JsonResponse({"question":"Hello"},safe=False)

def submitans(request):
    print(request.POST)
    if request.method=="POST":
        question=request.POST.get("quesid")
        print(question)
        if question not in request.POST:
            return JsonResponse({"please": "don't play with items or you will be disqualified."})
        else:
            solution=request.POST.get(question)
            try:
                current = OnGoingcontest.objects.get(pk=1)
                participant=Participation.objects.get(username=request.user,contest_part=current.on_going)
                ques = Ques_ans.objects.get(pk=question)
                quiz_score,made=QuizScore.objects.get_or_create(participation=participant,ques_id=ques,user_id=request.user)
                if made:
                    if int(ques.corr_ans)==int(solution):
                        quiz_score.solved=1
                quiz_score.save()
                return redirect("http://18.191.228.240:8090/contest/quiz/")
            except Exception as e:
                return HttpResponse("<h1>Please login again. Click  <a href='home/'>here</a> to do again.</h1>"+e )
            print(solution)
    return JsonResponse({"ok":"done"})

def logoutuser(request):
    logout(request)
    return redirect("/home/")